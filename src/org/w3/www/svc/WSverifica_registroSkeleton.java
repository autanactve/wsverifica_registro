
/**
 * WSverifica_registroSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package org.w3.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSverifica_registroSkeleton java skeleton for the axisService
     */
    public class WSverifica_registroSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSverifica_registroLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreTabla
                                     * @param nombreCampo
                                     * @param valorCampo
         */
        

                 public java.math.BigInteger verificaRegistro
                  (
                  java.lang.String nombreTabla,java.lang.String nombreCampo,java.lang.String valorCampo
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreTabla",nombreTabla);params.put("nombreCampo",nombreCampo);params.put("valorCampo",valorCampo);
		try{
		
			return (java.math.BigInteger)
			this.makeStructuredRequest(serviceName, "verificaRegistro", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    