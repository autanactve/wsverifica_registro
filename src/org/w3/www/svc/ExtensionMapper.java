
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package org.w3.www.svc;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "WSverifica_registro-RQ-Type".equals(typeName)){
                   
                            return  org.w3.www.gis.WSverifica_registroRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "WSverifica_registro-RS-Type".equals(typeName)){
                   
                            return  org.w3.www.gis.WSverifica_registroRSType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    